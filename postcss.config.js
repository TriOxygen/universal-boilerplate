const postcssPresetEnv = require('postcss-preset-env');
const paths = require('./config/paths');

module.exports = {
  plugins: [
    require('postcss-import')({
      path: paths.srcStyle,
    }),
    // require('postcss-css-reset'),
    require('postcss-nested')(),
    // require('postcss-custom-properties')(),
    require('postcss-flexbugs-fixes')(),
    require('autoprefixer')({
      browsers: ['last 3 versions', 'IE >= 9', 'Edge <= 15'],
    }),
    // require('postcss-custom-properties')(),
    // require('postcss-assets')({
    //   basePath: './assets',
    // }),
    // require('postcss-normalize')(),
    require('postcss-calc')(),
    postcssPresetEnv({
      browsers: 'last 2 versions',
      stage: 2,
      features: {
        'nesting-rules': true,
        'color-mod-function': {
          unresolved: 'warn',
        },
      },
    }),
  ],
  sourceMap: true,
};
