//@flow
import theme from 'styles/theme';
import { css } from 'emotion';

const { breakpoints } = theme;

const media = {
  sm: (...args) => css`
    @media (max-width: ${breakpoints.sm}px) {
      ${css(...args)};
    }
  `,
  md: (...args) => css`
    @media (min-width: ${breakpoints.md}px) {
      ${css(...args)};
    }
  `,
  lg: (...args) => css`
    @media (min-width: ${breakpoints.lg}px) {
      ${css(...args)};
    }
  `,
  xl: (...args) => css`
    @media (min-width: ${breakpoints.xl}px) {
      ${css(...args)};
    }
  `,
};

export default media;
