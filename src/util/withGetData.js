// @flow
import { lifecycle, withHandlers, compose } from 'recompose';

type GetDataT = ({}) => any;

export const withGetData = (getData: GetDataT) =>
  compose(
    withHandlers({
      getData,
    }),
    lifecycle({
      componentDidMount() {
        const { getData } = this.props;
        getData();
      },
    })
  );

export default withGetData;
