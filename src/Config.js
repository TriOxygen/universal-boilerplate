//@flow

const { API_HOST } = process.env;

export const API = {
  base: `http://${API_HOST}/rest/V1`,
  token: 'u1eb7cgcqmw9stfbjxor2pd8qxovsrir',
};

export default {};
