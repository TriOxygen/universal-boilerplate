// @flow
/* eslint-disable no-param-reassign */
import createReducer from 'store/createReducer';
import OverviewActionTypes from 'actions/overview.actions';

export const createOverviewTemplate = () => ({
  lastFetch: 0,
  selected: {},
  selectAll: false,
  isFetching: false,
  itemCount: 0,
  page: 1,
  items: [],
});

export const initialState = {};

const ensureOverview = (draft, overviewType) => {
  if (!draft[overviewType]) {
    draft[overviewType] = createOverviewTemplate();
  }
  return draft[overviewType];
};

export default createReducer(initialState, {
  [OverviewActionTypes.SET_SELECT_ALL]: (draft, { overviewType, selectAll }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.selectAll = selectAll;
  },
  [OverviewActionTypes.SET_ACTION]: (draft, { entityType, action }) => {
    const entity = ensureOverview(draft, entityType);
    entity.action = action;
  },
  [OverviewActionTypes.SELECT]: (draft, { overviewType, itemId }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.selected[itemId] = true;
  },
  [OverviewActionTypes.UNSELECT]: (draft, { overviewType, itemId }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.selected[itemId] = false;
  },
  [OverviewActionTypes.FETCH_START]: (draft, { overviewType }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.isFetching = true;
  },
  [OverviewActionTypes.SET_SELECT_ALL]: (draft, { overviewType, selectAll }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.selectAll = selectAll;
  },
  [OverviewActionTypes.FETCH_SUCCESS]: (draft, { overviewType, page, result }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.isFetching = false;
    overview.pristine = false;
    overview.items = result.items;
    overview.isFetching = false;
    overview.lastFetch = new Date();
    overview.itemCount = result.total_count;
    overview.page = page;
  },
  [OverviewActionTypes.FETCH_FAIL]: (draft, { overviewType }) => {
    const overview = ensureOverview(draft, overviewType);
    overview.isFetching = false;
  },
});
