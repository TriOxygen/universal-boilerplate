import { combineReducers } from 'redux';
import app from 'reducers/app.reducer';
import entity from 'reducers/entity.reducer';
import overview from 'reducers/overview.reducer';

const rootReducer = combineReducers({
  entity,
  overview,
  app,
});

export default rootReducer;
