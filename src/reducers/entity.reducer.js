// @flow
/* eslint-disable no-param-reassign */
import createReducer from 'store/createReducer';
import { EntityActionTypes } from 'actions';

export const createTemplate = () => ({
  items: {},
  fetchLoading: false,
  action: null,
  actionEntityId: null,
});

export const initialState = {};

const ensureEntityType = (draft, entityType) => {
  if (!draft[entityType]) {
    draft[entityType] = createTemplate();
  }
  return draft[entityType];
};

export const consumeEntities = (draft: {}, action: {}): void => {
  const { entities } = action;
  if (entities) {
    Object.keys(entities).forEach((entityType) => {
      const entity = ensureEntityType(draft, entityType);
      const objects = entities[entityType];
      Object.keys(objects).forEach((id) => {
        const right = objects[id];
        entity.items[id] = entity.items[id] || {};
        Object.keys(right).forEach((key) => {
          if (typeof right[key] !== 'undefined') {
            entity.items[id][key] = right[key];
          }
        });
      });
    });
  }
};

export default createReducer(initialState, {
  [EntityActionTypes.CREATE_SUCCESS]: (draft, { entityType }) => {
    const entity = ensureEntityType(draft, entityType);
    entity.action = null;
  },
  [EntityActionTypes.SET_ACTION]: (draft, { entityType, action }) => {
    const entity = ensureEntityType(draft, entityType);
    entity.action = action;
  },
  [EntityActionTypes.SET_ACTION_ENTITY]: (draft, { entityType, actionEntityId }) => {
    const entity = ensureEntityType(draft, entityType);
    entity.actionEntityId = actionEntityId;
  },
  [EntityActionTypes.SET_ACTION_AND_ENTITY]: (draft, { entityType, action, actionEntityId }) => {
    const entity = ensureEntityType(draft, entityType);
    entity.actionEntityId = actionEntityId;
    entity.action = action;
  },
  [EntityActionTypes.SET_VALUE]: (draft, { entityType, key, value }) => {
    const entity = ensureEntityType(draft, entityType);
    entity[key] = value;
  },
  default: consumeEntities,
});
