// @flow
/* eslint-disable no-param-reassign */
import createReducer from 'store/createReducer';
import AppActions from 'actions/app.actions';

export const initialState = {
  init: false,
};

export default createReducer(initialState, {
  [AppActions.INIT]: (draft) => {
    draft.init = true;
  },
});
