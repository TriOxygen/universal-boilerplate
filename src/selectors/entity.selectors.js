// @flow
import { createSelector } from 'reselect';
import { createTemplate } from 'reducers/entity.reducer';

const emptyTemplate = createTemplate();

export const makeGetItemsForEntityType = (entityType: string) => {
  return createSelector((state) => (state.entity[entityType] || emptyTemplate).items, (items) => items);
};

export const makeGetItemForEntityType = (entityType: string) => {
  return createSelector(
    (state) => (state.entity[entityType] || emptyTemplate).items,
    (state, entityId) => entityId,
    (items, entityId) => items[entityId]
  );
};

export const makeGetActionForEntityType = (entityType: string) =>
  createSelector((state) => (state.entity[entityType] || emptyTemplate).action, (action) => action);
