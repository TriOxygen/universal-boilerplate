// @flow
import { createSelector } from 'reselect';
import { Entity } from 'Constants';
import { makeGetItemsForEntityType } from 'selectors/entity.selectors';

const getCategoryEntities = makeGetItemsForEntityType(Entity.Category);
const getParentCategories = makeGetItemsForEntityType(Entity.Category, (category) => category.parent_id === 1);

export const getParentCategory = createSelector(getParentCategories, (entities) => {
  const categoryIds = Object.keys(entities).filter((id) => {
    return entities[id].parent_id === 1;
  });
  return entities[categoryIds[0]];
});

export const getMainCategories = createSelector(getCategoryEntities, getParentCategory, (entities, parentCategory) => {
  if (!parentCategory) return [];
  return parentCategory.children_data.map((childId) => entities[childId]).filter((category) => category.is_active);
});

export const getCategories = createSelector(getCategoryEntities, (entities) => {
  return Object.keys(entities).map((id) => entities[id]);
});
