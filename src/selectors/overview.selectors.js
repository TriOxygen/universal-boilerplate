// @flow
import { createSelector } from 'reselect';
import { createOverviewTemplate } from 'reducers/overview.reducer';

const emptyTemplate = createOverviewTemplate();

export const makeGetItemsForOverview = (overviewType: string) => {
  return createSelector(
    (state) => (state.overview[overviewType] ? state.overview[overviewType].items : []),
    (items) => items
  );
};

export const makeGetOverview = (overviewType: string) => {
  return createSelector((state) => state.overview[overviewType] || emptyTemplate, (overview) => overview);
};
