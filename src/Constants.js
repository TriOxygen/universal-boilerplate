//@flow

export const Entity = {
  Product: 'product',
  Category: 'category',
};

export const Action = {
  Create: 'create',
  Edit: 'edit',
  Delete: 'delete',
};

export const Overviews = {
  Product: 'product',
  Category: 'category',
};

export const Endpoints = {
  Category: 'categories',
  Product: 'products',
};
