import React from 'react';
import { hydrate } from 'react-dom';
import { configureStore } from 'store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import theme from 'styles/theme';
import { ThemeProvider } from '@smooth-ui/core-em';
import App from './components/App';

const initialData = window.__INITIAL_DATA__ ? window.__INITIAL_DATA__ : {};

const { store } = configureStore({
  initialState: initialData.state,
  middleware: [],
});

const renderApp = (Application) =>
  hydrate(
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <BrowserRouter>
          <Application />
        </BrowserRouter>
      </Provider>
    </ThemeProvider>,
    document.getElementById('root')
  );

if (process.env.NODE_ENV === 'development' && module.hot) {
  if (module.hot) {
    module.hot.accept();
  }
}

renderApp(App);
