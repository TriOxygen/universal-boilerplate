//@flow
import axios from 'axios';
import { API } from 'Config';
import qs from 'qs';

export default {
  get: (url, params) => {
    return axios({
      url: `${API.base}/${url}?${qs.stringify(params)}`,
      method: 'GET',
      // params: params,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        authorization: `Bearer ${API.token}`,
      },
    }).then(async (response) => {
      if (response.status >= 400) {
        throw new Error();
      }
      return response.data;
    });
  },
};
