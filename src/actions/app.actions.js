// @flow
const ActionTypes = {
  INIT: 'app/init',
};

export const init = () => ({
  type: ActionTypes.INIT,
});

export default ActionTypes;
