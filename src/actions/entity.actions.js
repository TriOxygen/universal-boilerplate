// @flow
import type { ItemIdType } from 'types/entity.types';

const ActionTypes = {
  SET_VALUE: '@entities/setValue',
  SET_ACTION: '@entities/setAction',
  SET_ACTION_ENTITY: '@entities/setActionEntity',
  SET_ACTION_AND_ENTITY: '@entities/setActionAndEntity',
  FETCH_REQUEST: '@entities/requestFetch',
  FETCH_START: '@entities/startFetch',
  FETCH_SUCCESS: '@entities/succeedFetch',
  FETCH_FAIL: '@entities/failFetch',
  CREATE_REQUEST: '@entities/requestCreate',
  CREATE_START: '@entities/startCreate',
  CREATE_SUCCESS: '@entities/succeedCreate',
  CREATE_FAIL: '@entities/failCreate',
};

export const requestFetch = (entityType: string, itemId: ItemIdType) => ({
  type: ActionTypes.FETCH_REQUEST,
  entityType,
  itemId,
});

export const startFetch = (entityType: string, itemId: ItemIdType) => ({
  type: ActionTypes.FETCH_START,
  entityType,
  itemId,
});

export const succeedFetch = (entityType: string, itemId: ItemIdType, item: {}, data: {}) => ({
  type: ActionTypes.FETCH_SUCCESS,
  entityType,
  itemId,
  item,
  ...data,
});

export const failFetch = (entityType: string, itemId: ItemIdType, error: string) => ({
  type: ActionTypes.FETCH_FAIL,
  entityType,
  itemId,
  error,
});

export const requestCreate = (entityType: string, form: string, data: {}) => ({
  type: ActionTypes.CREATE_REQUEST,
  entityType,
  data,
});

export const startCreate = (entityType: string, data: {}) => ({
  type: ActionTypes.CREATE_START,
  entityType,
  data,
});

export const succeedCreate = (entityType: string, data: {}) => ({
  type: ActionTypes.CREATE_SUCCESS,
  entityType,
  ...data,
});

export const failCreate = (entityType: string, data: {}, error: string) => ({
  type: ActionTypes.CREATE_FAIL,
  entityType,
  data,
  error,
});

export const setAction = (entityType: string, action: string) => ({
  type: ActionTypes.SET_ACTION,
  entityType,
  action,
});

export const clearAction = (entityType: string) => ({
  type: ActionTypes.SET_ACTION,
  entityType,
  action: null,
});

export const setActionEntity = (entityType: string, actionEntityId: string) => ({
  type: ActionTypes.SET_ACTION_ENTITY,
  entityType,
  actionEntityId,
});

export const setValue = (entityType: string, key: string, value: any) => ({
  type: ActionTypes.SET_VALUE,
  entityType,
  key,
  value,
});

export const setActionAndEntity = (entityType: string, action: string, actionEntityId: string) => ({
  type: ActionTypes.SET_ACTION_ENTITY,
  entityType,
  action,
  actionEntityId,
});

export default ActionTypes;
