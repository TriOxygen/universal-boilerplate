import styled from 'react-emotion';
import { mq } from 'util';
import {
  space,
  color,
  width,
  fontSize,
  fontWeight,
  textAlign,
  lineHeight,
  display,
  borderRadius,
  borderColor,
  borders,
  alignItems,
  alignContent,
  justifyContent,
  flexWrap,
  flexBasis,
  flexDirection,
  flex,
  justifySelf,
  alignSelf,
} from 'styled-system';

const Container = styled.div`
  font-family: ${({ theme }) => theme.fontFamily};
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  box-sizing: border-box;
  ${({ fluid }) => mq.sm`
    max-width: ${fluid ? '100%' : '540px'};
  `}
  ${({ fluid }) => mq.md`
    max-width: ${fluid ? '100%' : '720px'};
  `}
  ${({ fluid }) => mq.lg`
    max-width: ${fluid ? '100%' : '960px'};
  `}
  ${({ fluid }) => mq.xl`
    max-width: ${fluid ? '100%' : '1140px'};
  `}
  ${({ shadow, theme }) => shadow && `box-shadow: ${theme.shadow[shadow]}`};
  ${space};
  ${width};
  ${color};
  ${fontSize};
  ${fontWeight};
  ${textAlign};
  ${lineHeight};
  ${display};
  ${borderRadius};
  ${borderColor};
  ${borders};
  ${alignItems};
  ${alignContent};
  ${justifyContent};
  ${flexWrap};
  ${flexBasis};
  ${flexDirection};
  ${flex};
  ${justifySelf};
  ${alignSelf};
`;

export default Container;
