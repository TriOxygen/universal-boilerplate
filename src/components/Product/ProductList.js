//@flow
import * as React from 'react';
import 'styles/theme.less';
import { compose, lifecycle, withHandlers } from 'recompose';
import { OverviewActions } from 'actions';
import { Overviews } from 'Constants';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { Card, Pagination, Segment } from 'semantic-ui-react';
import { makeGetItemsForOverview, makeGetOverview } from 'selectors/overview.selectors';
import { withGetData } from 'util';
import ProductCard from './ProductCard';

type PropsType = {
  products: Array<string>,
  handlePageChange: (event: Event, data: {}) => void,
  amountOfPages: number,
  currentPage: number,
};

const ProductList = ({ products, handlePageChange, amountOfPages, currentPage }: PropsType) => {
  return (
    <React.Fragment>
      <Pagination
        attached
        firstItem={null}
        lastItem={null}
        defaultActivePage={currentPage}
        totalPages={amountOfPages}
        onPageChange={handlePageChange}
      />
      <Segment attached>
        <Card.Group itemsPerRow={4} stackable centered>
          {products.map((productId) => (
            <ProductCard productId={productId} key={productId} />
          ))}
        </Card.Group>
      </Segment>
      <Pagination
        attached="bottom"
        firstItem={null}
        lastItem={null}
        defaultActivePage={currentPage}
        totalPages={amountOfPages}
        onPageChange={handlePageChange}
      />
    </React.Fragment>
  );
};

const overviewType = Overviews.Product;

const getProducts = makeGetItemsForOverview(overviewType);
const getOverview = makeGetOverview(overviewType);

const mapDispatchToProps = (
  dispatch,
  {
    match: {
      params: { categoryId },
    },
  }
) => ({
  requestFetch: (page = 1) => {
    dispatch(OverviewActions.requestFetch(overviewType, page, categoryId));
  },
});

const mapStateToProps = (state) => {
  const overview = getOverview(state, overviewType);
  return {
    products: getProducts(state),
    amountOfPages: Math.ceil(overview.itemCount / 10),
    currentPage: overview.page,
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withGetData(({ requestFetch }) => () => {
    requestFetch();
  }),
  lifecycle({
    componentDidUpdate(prevProps) {
      if (prevProps.match.params.categoryId !== this.props.match.params.categoryId) {
        this.props.requestFetch();
      }
    },
  }),
  withHandlers({
    handlePageChange: ({ requestFetch }) => (event, data) => {
      requestFetch(data.activePage);
    },
  })
)(ProductList);
