//@flow
import * as React from 'react';
import 'styles/theme.less';
import { compose } from 'recompose';
import { Overviews } from 'Constants';
import { withRouter } from 'react-router';
import { Image, Label } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { makeGetItemForEntityType } from 'selectors/entity.selectors';
import Card from 'components/Card/Card';

type PropsType = {
  product: {},
};

const Product = ({ product }: PropsType) => {
  const hasRibbon = product.attributes.new || product.attributes.sale;
  return (
    <Card p={2} m={2}>
      <div extra>
        {hasRibbon && (
          <Label ribbon color="green">
            {product.attributes.sale ? 'Sale-' : null}
            {product.attributes.new ? 'New' : null}
          </Label>
        )}
      </div>
      <div textAlign="center">
        <Image src={`//local.magento/pub/media/catalog/product/${product.attributes.thumbnail}`} size="small" />
      </div>
      <div extra>
        <h4>{product.name}</h4>
      </div>
    </Card>
  );
};

const overviewType = Overviews.Product;

const getProduct = makeGetItemForEntityType(overviewType);

const mapStateToProps = (state, { productId }) => ({
  product: getProduct(state, productId),
});

export default compose(
  connect(mapStateToProps),
  withRouter
)(Product);
