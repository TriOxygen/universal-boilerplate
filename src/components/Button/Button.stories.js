// @flow
import * as React from 'react';
// import { boolean } from '@storybook/addon-knobs/react';
import Button from './Button';

type StoryT = {
  add(string, () => React.Node): void,
};

const stories = (storybook: StoryT) => {
  storybook.add('Open and CLose', () => (
    <div style={{ minHeight: '100vh', backgroundColor: '#E0E0E0' }}>
      <Button />
    </div>
  ));
};

export default {
  name: 'Button',
  stories,
};
