//@flow
import * as React from 'react';
import cx from 'classnames';
import css from './Button.css';

const cn = cx(css.button, css.default);

const Button = () => <button className={cn}>Button</button>;

export default Button;
