//@flow
import * as React from 'react';
import 'styles/theme.less';
import { Menu } from 'semantic-ui-react';
import { compose } from 'recompose';
import { OverviewActions } from 'actions';
import { Overviews } from 'Constants';
import { getMainCategories } from 'selectors/category.selectors';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router';
import CategoryMenu from 'components/Category/CategoryMenu';
import Products from 'components/Product/ProductList';
import { withGetData } from 'util';
// import styled from 'react-emotion';
// import { space, color } from 'styled-system';
// import { Grid, Row, Col } from '@smooth-ui/core-em';
import Container from 'components/Container/Container';
// import Card from 'components/Card/Card';
// import css from './App.css';

type PropsType = {
  categories: Array<{}>,
};

const App = ({ categories = [] }: PropsType) => {
  return (
    <Container p={3}>
      <Menu attached="top" size="tiny">
        {categories.map((category) => {
          return <CategoryMenu categoryId={category.id} key={category.id} />;
        })}
      </Menu>
      <Switch>
        <Route exact path="/products/:categoryId" component={Products} />
      </Switch>
    </Container>
  );
};

const mapDispatchToProps = {
  requestFetch: OverviewActions.requestFetch,
};

const mapStateToProps = (state) => ({
  categories: getMainCategories(state),
});

const overviewType = Overviews.Category;

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withGetData(({ requestFetch }) => () => {
    requestFetch(overviewType);
  })
)(App);
