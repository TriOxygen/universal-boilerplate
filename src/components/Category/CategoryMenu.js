// @flow
import * as React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import { makeGetItemForEntityType } from 'selectors/entity.selectors';
import { Link } from 'react-router-dom';
import { Entity } from 'Constants';

type PropsT = {
  category: {},
};

const makeMapStateToProps = () => {
  const getCategory = makeGetItemForEntityType(Entity.Category);
  return (state, { categoryId }) => {
    return {
      category: getCategory(state, categoryId),
    };
  };
};

const hoc = compose(connect(makeMapStateToProps));

const CategoryMenu = hoc(({ category }: PropsT) => {
  if (!category) {
    return null;
  }
  const hasChildren = category.children_data ? category.children_data.length > 0 : false;
  if (!hasChildren) {
    return (
      <Dropdown.Item as={Link} to={`/products/${category.id}`}>
        {category.name}
      </Dropdown.Item>
    );
  }
  // console.log(category);
  return (
    <Dropdown openOnFocus item text={category.name}>
      <Dropdown.Menu>
        {category.children_data.map((subCategoryId) => {
          return <CategoryMenu key={subCategoryId} categoryId={subCategoryId} />;
        })}
      </Dropdown.Menu>
    </Dropdown>
  );
});

export default CategoryMenu;
