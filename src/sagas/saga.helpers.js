// @flow
import { OverviewActions, EntityActions } from 'actions';
import { put, fork } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form';

import type { ItemIdType } from 'types/entity.types';

export const createFetchListTask = (overviewType: string, entityType: string, fetchData: () => {}) => {
  function* fetchList(searchCriteria, page): Generator<*, *, *> {
    try {
      yield put(OverviewActions.startFetch(overviewType));
      const data = yield fetchData(searchCriteria, page);
      if (!data) {
        const error = new Error('No data');
        throw error;
      }
      // let items = [];
      // if (data && data.entities && data.entities[entityType]) {
      //   items = Object.keys(data.entities[entityType]);
      // }
      yield put(OverviewActions.succeedFetch(overviewType, page, data));
    } catch (e) {
      yield put(OverviewActions.failFetch(overviewType, e.message));
    }
  }

  return function* watch({ overviewType: actionOverviewType, searchCriteria, page }) {
    if (overviewType === actionOverviewType) {
      yield fork(fetchList, searchCriteria, page);
    }
  };
};

export const createFetchEntityTask = (entityType: string, fetchData: (ItemIdType) => {}) => {
  function* fetch(itemId): Generator<*, *, *> {
    try {
      yield put(EntityActions.startFetch(entityType, itemId));
      const data = yield fetchData(itemId);
      let item = null;
      if (data.entities && data.entities[entityType] && data.result) {
        item = data.entities[entityType][data.result];
      }
      yield put(EntityActions.succeedFetch(entityType, itemId, item, data));
    } catch (e) {
      console.error(e);
      yield put(EntityActions.failFetch(entityType, itemId, e.message));
    }
  }

  return function* watch({ entityType: actionEntityType, itemId }) {
    if (entityType === actionEntityType) {
      yield fork(fetch, itemId);
    }
  };
};

export const createCreateEntityTask = (entityType: string, createRequest: ({}) => {}) => {
  function* fetch(data: {}, form): Generator<*, *, *> {
    try {
      yield put(EntityActions.startCreate(entityType));
      yield put(startSubmit(form));
      const response = yield createRequest(data);
      yield put(EntityActions.succeedCreate(entityType, response));
      yield put(stopSubmit(form));
    } catch (e) {
      console.error(e);
      yield put(EntityActions.failCreate(entityType, data, e.message));
      yield put(stopSubmit(form, { _error: e.message }));
    }
  }

  return function* watch({ entityType: actionEntityType, data, form }) {
    if (entityType === actionEntityType) {
      yield fork(fetch, data, form);
    }
  };
};

export default createFetchListTask;
