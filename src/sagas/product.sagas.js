// @flow
import { takeLatest, takeEvery } from 'redux-saga/effects';
import api from 'api';
import { normalize } from 'normalizr';
import { ProductList } from 'schemas';
import { OverviewActionTypes, EntityActionTypes } from 'actions';
import { Overviews, Entity, Endpoints } from 'Constants';
import { createFetchListTask, createFetchEntityTask, createCreateEntityTask } from './saga.helpers';

declare var process: {
  browser: boolean,
};

const watchCreate = createCreateEntityTask(Entity.Product, () => {});

const watchFetch = createFetchEntityTask(Entity.Product, () => {});

const watchOverviewChange = createFetchListTask(Overviews.Product, Entity.Product, (categoryId, page) => {
  return api
    .get(Endpoints.Product, {
      search_criteria: {
        filter_groups: [
          {
            filters: [
              {
                field: 'category_id',
                value: categoryId,
                condition_type: 'eq',
              },
              {
                field: 'visibility',
                value: 4,
                condition_type: 'eq',
              },
            ],
          },
        ],
        sort_orders: [
          // {
          //   field: 'created_at',
          //   direction: 'DESC',
          // },
          {
            field: 'id',
            direction: 'DESC',
          },
        ],
        page_size: 10,
        current_page: page,
      },
    })
    .then((data) => {
      return normalize(data, ProductList);
    });
});

export default [
  takeLatest(EntityActionTypes.CREATE_REQUEST, watchCreate),
  takeLatest(EntityActionTypes.FETCH_REQUEST, watchFetch),
  takeEvery(OverviewActionTypes.FETCH_REQUEST, watchOverviewChange),
];
