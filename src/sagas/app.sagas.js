// @flow
import { takeLatest } from 'redux-saga/effects';
import { AppActionTypes } from 'actions';

function* init() {}

export default [takeLatest(AppActionTypes.INIT, init)];
