// @flow
import { takeLatest, takeEvery } from 'redux-saga/effects';
import api from 'api';
import { normalize } from 'normalizr';
import { CategorySchema } from 'schemas';
import { OverviewActionTypes, EntityActionTypes } from 'actions';
import { Overviews, Entity, Endpoints } from 'Constants';
import { createFetchListTask, createFetchEntityTask, createCreateEntityTask } from './saga.helpers';

declare var process: {
  browser: boolean,
};

const watchCreate = createCreateEntityTask(Entity.Category, () => {});

const watchFetch = createFetchEntityTask(Entity.Category, () => {});

const watchOverviewChange = createFetchListTask(Overviews.Category, Entity.Category, async () => {
  return api.get(Endpoints.Category).then((data) => {
    return normalize(data, CategorySchema);
  });
});

export default [
  takeLatest(EntityActionTypes.CREATE_REQUEST, watchCreate),
  takeLatest(EntityActionTypes.FETCH_REQUEST, watchFetch),
  takeEvery(OverviewActionTypes.FETCH_REQUEST, watchOverviewChange),
];
