//@flow
import appSagas from './app.sagas';
import categorySagas from './category.sagas';
import productSagas from './product.sagas';

export default () => [...appSagas, ...categorySagas, ...productSagas];
