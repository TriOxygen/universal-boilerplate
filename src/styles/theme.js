//@flow
import { theme } from '@smooth-ui/core-em';

console.log(theme);

const shadow = [
  null,
  '0 1px 3px 0 rgba(0, 0, 0, 0.11)',
  '0 2px 6px 0 rgba(0, 0, 0, 0.06)',
  '0 5px 14px 0 rgba(0, 0, 0, 0.08)',
  '0 2px 24px 0 rgba(0,0,0,.07)',
];

export default {
  ...theme,
  shadow,
};
