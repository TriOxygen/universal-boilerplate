// @flow
import { schema } from 'normalizr';
import { Entity } from 'Constants';

export const CategorySchema = new schema.Entity(
  Entity.Category,
  {},
  {
    idAttribute: 'id',
  }
);

CategorySchema.define({
  children_data: [CategorySchema],
});

export const CategoryList = new schema.Array(CategorySchema);
