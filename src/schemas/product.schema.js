// @flow
import { schema } from 'normalizr';
import { Entity } from 'Constants';

const getAttributes = (customAttributes) =>
  customAttributes.reduce((accum, attribute) => {
    accum[attribute.attribute_code] = attribute.value;
    return accum;
  }, {});

export const ProductSchema = new schema.Entity(
  Entity.Product,
  {},
  {
    processStrategy: (product) => ({
      ...product,
      attributes: getAttributes(product.custom_attributes),
      custom_attributes: undefined,
    }),
  }
);

export const ProductList = new schema.Object({
  items: [ProductSchema],
});
